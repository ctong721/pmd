# pmd

#### 介绍
 
Markdown文档发布工具。

将程序执行目录下的Markdown文件解析为html，并提供http或https协议发布。 

#### 使用说明

window系统：

```
pmd_win64.exe --help
----------------------------------------------------------
- ©2021 CT 作者不承担任何法律和安全等责任和义务 版本v1.0
- 需要帮助时，在命令行下执行pmd -h
- PMD当前提供页面样式自定义、ssl、手机页面自适应、端口自定义、目录列表等功能
Usage of pmd_win64.exe:
  -c string
        样式和脚本配置，可以实现自定义样式或js脚本,不配置此项的情况下，使用默认样式
  -l    显示目录的文件列表，默认打开；关闭后访问根，默认访问index.md (default true)
  -m    支持手机页面自适应，默认打开 (default true)
  -p int
        端口配置，默认8000端口 (default 8000)
  -s    支持ssl，需要密钥文件，默认关闭
```

linux系统：

```pmd_linux_amd64 --help```

macOS系统：

```pmd_darwin_amd64 --help```

linux系统arm架构：

```pmd_linux_arm64 --help```
